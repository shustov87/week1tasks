﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace Week1Tasks
{
    /// <summary>
    /// Tasks for day 4
    /// 
    /// 1)      Create class A with some public property and try to apply different access modifiers to its getter and setter. 
    ///         Try to aply different access modifiers separately to property,getter and setter. What is the result?
    /// 2)      What will be result of each pieces of code?
    /// 3)      Create a static int field in your class. Create a method which modifies passed by ref int parameter. Try pass static field to this method. What is the result? Explain it.
    /// 4)      Add private readonly fileld of int type to your class. Try to pass this field to the method from practice 3. What the result?
    /// 5)      For the practice 5 create a public property with get/set accessor for readonly field. Try to use this property. What is the result?
    /// 6)      Create an exaple of "property with parameters"
    /// 7)      Create an example of method with variable amount of parameters
    /// 8)      Create a speed test for switch/if else/?: operators. (You may use StopWatch class for time calculations) 
    /// </summary>
    class Day4
    {
       
        public static void DoTask()
        {
            Console.WriteLine("Task 8.");
            Console.WriteLine("speed test for switch/if else/?: operators.");
            Console.WriteLine("*results can be different for Debug/Release build");
           
            // test 100000 times with 1,5,10
            TestSelectionStatements(100000, 1);
            TestSelectionStatements(100000, 5);
            TestSelectionStatements(100000, 10);
        }

        private static void TestSelectionStatements(int count, int testValue)
        {
            var stopwatch = new Stopwatch();
            Console.WriteLine("====================");
            Console.WriteLine("Check {0} times, with value {1}", count, testValue);

            stopwatch.Start();
            for (int i = 0; i < count; i++)
            {
                TestIf(5);
            }
            stopwatch.Stop();
            Console.WriteLine("if construction: {0}", stopwatch.ElapsedTicks);
            
            stopwatch.Reset();
            stopwatch.Start();
            for (int i = 0; i < count; i++)
            {
                TestSwitch(5);
            }
            stopwatch.Stop();
            Console.WriteLine("switch construction: {0}", stopwatch.ElapsedTicks);
        }

        private static string TestIf(int value)
        {
            string result = string.Empty;
            if (value == 1)
            {
                result = "first";
            }
            else if (value == 2)
            {
                result = "second";
            }
            else if (value == 3)
            {
                result = "third";
            }
            else if (value == 4)
            {
                result = "fourth";
            }
            else if (value == 5)
            {
                result = "fifth";
            }

            return result;
        }

        private static string TestSwitch(int value)
        {
            string result = string.Empty;
            switch (value)
            {
                case 1:
                    result = "first";
                    break;
                case 2:
                    result = "second";
                    break;
                case 3:
                    result = "third";
                    break;
                case 4:
                    result = "fourth";
                    break;
                case 5:
                    result = "fifth";
                    break;
                default:
                    break;
            }
           
            return result;
        }
    }
}

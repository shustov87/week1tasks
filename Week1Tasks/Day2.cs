﻿using System;

namespace Week1Tasks
{
    class Day2
    {
        public static void DoTask()
        {
            Task1();
            Task2();
            Task3();
        }

        private static void Task1()
        {
            Console.WriteLine("Task 1. Write a simple program as an example of type overlow. Using checked/unchecked keywords show how to manage compiler's reaction on type overflow.");
            Console.WriteLine();

            const byte addVal = 10;
            byte maxByte = byte.MaxValue;
            Console.WriteLine("Try add {0} to max byte value({1}) ", addVal, maxByte);

            Console.WriteLine("checked keyword test");
            try
            {
                checked
                {
                    byte result = (byte)(maxByte + addVal);
                    Console.WriteLine(result);
                }
            }
            catch (OverflowException ex)
            {
                Console.WriteLine(ex.Message);
            }

            Console.WriteLine("unchecked keyword test");
            maxByte = byte.MaxValue;
            unchecked
            {
                byte result = (byte)(maxByte + addVal);
                Console.WriteLine(result);
            }

            Console.WriteLine();
        }

        private static void Task2()
        {
            Console.WriteLine("Task 2. Try to cast (implicitly & explicitly) Int32 value to different types (Int64, byte, double, short). How compiler will react on each casting?");
            Console.WriteLine();

            Int32 val = 10000;
            Int64 valInt64 = val;
            byte valByte = (byte)val;
            double valDouble = val;
            short valShort = (short)val;
            Console.WriteLine("Value {0}\nInt64 {1}\nbyte {2}\ndouble {3}\nshort {4}\n", val, valInt64, valByte, valDouble, valShort);

            Console.WriteLine();
        }

        private static void Task3()
        {
            Console.WriteLine("Task 3. Write a simple program which demonstrates boxing/unboxing");
            Console.WriteLine();

            const int firstVal = 1;
            const int secondVal = 2;
            Console.WriteLine("Let's add {0} and {1}", firstVal, secondVal);
            Console.WriteLine("{0}+{1}={2}", firstVal, secondVal, firstVal + secondVal);

            Object obj = secondVal;
            Console.WriteLine("after boxing {0}", firstVal + secondVal.ToString());

            int val = firstVal + (int)obj;
            Console.WriteLine("after unboxing {0}", val);

            Console.WriteLine();
        }

    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Week1Tasks
{
    /// <summary>
    /// Tasks for day 5 
    /// 
    ///    1. Create a simple method which takes enumeration as parameter and display passed value on console.
    ///    2. Do the same as in task 1, but for bit flags.
    ///    3. Write a simple method which takes two arguments. First is sequence of bit flags, second is bit flag to search in first sequence. Method should return true in case if sequence contains needed bit flag.
    ///    4. Write a method which will perform some action in case if condition is true. Condition and Action should be passed as parameters (delegates).
    ///    5. Create a class-wrapper for collection. Create overloaded operator+ for this class. The result of operator+ should be a class with collection - result of concatenation of two collections.
    ///    6. Create a custom type and provide overloaded operators > , < , == and != for it.
    /// </summary>
    class Day5
    {
        #region First
        
        public void EnumToString(IEnumerable collection)
        {
            foreach (var item in collection)
            {
                Console.WriteLine(item);
            }
        }

        #endregion

        #region Second&Third

        [Flags]
        internal enum DaysOfWeek
        {
            Sunday = 0x0,
            Monday = 0x1,
            Tuesday = 0x2,
            Wednesday = 0x4,
            Thursday = 0x8,
            Friday = 0x10,
            Saturday = 0x20
        }

        public static void PrintDays(DaysOfWeek daysOfWeek)
        {
            Console.WriteLine(daysOfWeek.ToString());
        }

        public static bool HasFlag(DaysOfWeek daysOfWeek, DaysOfWeek flag)
        {
            //return daysOfWeek.HasFlag(flag); // available from from .net 4.0
            return (daysOfWeek & flag) == flag;
        }

        #endregion
        
        #region Fourth

        public class ActionHelper
        {
            public delegate bool Condition();
            public delegate void Action();

            public void DoAction(Condition condition, Action action)
            {
                if (condition())
                    action();
            }
        }

        #endregion
        
        #region Fifth

        public class CustomList<T> : List<T>
        {
            public static CustomList<T> operator +(CustomList<T> c1, CustomList<T> c2)
            {
                if (c1 == null || !c1.Any())
                {
                    throw new ArgumentNullException();
                }

                if (c2 == null || !c2.Any())
                {
                    throw new ArgumentNullException();
                }
                var result = new CustomList<T>();
                result.AddRange(c1);
                result.AddRange(c2);
                return result;
            }
        }

        #endregion

        #region Sixth

        public class FruitsBag
        {
            public double Apple { get; set; }
            public double Banana { get; set; }
            public double Strawberry { get; set; }

            public static bool operator !=(FruitsBag x, FruitsBag y)
            {
                if (object.ReferenceEquals(x, null)) return false;
                if (object.ReferenceEquals(y, null)) return false;

                return x.Apple != y.Apple || x.Banana != y.Banana || x.Strawberry != y.Strawberry;
            }

            public static bool operator ==(FruitsBag x, FruitsBag y)
            {
                if (object.ReferenceEquals(x, null)) return false;
                if (object.ReferenceEquals(y, null)) return false;

                return !(x != y);
            }

            public static bool operator >(FruitsBag x, FruitsBag y)
            {
                if (object.ReferenceEquals(x, null)) return false;
                if (object.ReferenceEquals(y, null)) return false;

                return x.Apple > y.Apple && x.Banana > y.Banana && x.Strawberry > y.Strawberry;
            }

            public static bool operator <(FruitsBag x, FruitsBag y)
            {
                if (object.ReferenceEquals(x, null)) return false;
                if (object.ReferenceEquals(y, null)) return false;

                return x.Apple < y.Apple && x.Banana < y.Banana && x.Strawberry < y.Strawberry;
            }
        }
        
        #endregion

        public static void DoTask()
        {
            Console.WriteLine("HasFlag: {0}", HasFlag(DaysOfWeek.Wednesday & DaysOfWeek.Sunday, DaysOfWeek.Monday));
            new ActionHelper().DoAction(() => DateTime.Now.Minute%2 == 0, () => Console.WriteLine("It was true!"));

            var bag1 = new FruitsBag { Apple = 1, Banana = 1, Strawberry = 1 };
            var bag2 = new FruitsBag { Apple = 1, Banana = 1, Strawberry = 1 };
            var bag3 = new FruitsBag { Apple = 2, Banana = 2, Strawberry = 2 };
            var bag4 = new FruitsBag { Apple = 1, Banana = 3, Strawberry = 1 };

            Console.WriteLine(bag1 == bag2);
            Console.WriteLine(bag1 == bag3);
            Console.WriteLine(bag1 < bag2);
            Console.WriteLine(bag1 < bag3);
            Console.WriteLine(bag1 < bag4);

        }
    }
}

﻿using System;
using System.Collections.Generic;

namespace Week1Tasks
{
    class Program
    {
        static void Main(string[] args)
        {
            NotifyUser();
            string curLine = string.Empty;
            ConsoleKeyInfo key;
            do
            {
                key = Console.ReadKey(true);
                if (key.Key == ConsoleKey.Backspace)
                {
                    if (key.Key == ConsoleKey.Backspace && curLine.Length > 0)
                    {
                        curLine = curLine.Substring(0, (curLine.Length - 1));
                        Console.Write("\b \b");
                    }
                }
                else
                {
                    if (key.Key == ConsoleKey.Enter)
                    {
                        int currentTask;
                        if (!int.TryParse(curLine, out currentTask) || !RunAction(currentTask))
                        {
                            Console.WriteLine("\nPlease input correct number!");
                        }
                        curLine = string.Empty;
                    }
                    else
                    {
                        double val;
                        if (double.TryParse(key.KeyChar.ToString(), out val))
                        {
                            curLine += key.KeyChar;
                            Console.Write(key.KeyChar);
                        }
                    }
                }
            }
            while (key.Key != ConsoleKey.Escape);
        }

        private static void NotifyUser()
        {
            Console.WriteLine("Choose any day from 2-5. Or press Esc to exit...");
        }

        private delegate void CurrentAction();

        private static readonly Dictionary<int, CurrentAction> ActionDictionary = new Dictionary<int, CurrentAction>
                            {
                              {2, new CurrentAction(Day2.DoTask)},
                              {3, new CurrentAction(Day3.DoTask)},
                              {4, new CurrentAction(Day4.DoTask)},
                              {5, new CurrentAction(Day5.DoTask)},
                            };

        private static bool RunAction(int currentTask)
        {
            if (ActionDictionary.ContainsKey(currentTask))
            {
                Console.WriteLine();
                ActionDictionary[currentTask].Invoke();
                Console.WriteLine();
                NotifyUser();
                return true;
            }

            return false;
        }
    }
}
